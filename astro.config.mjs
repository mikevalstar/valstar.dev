import { defineConfig } from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';
import react from '@astrojs/react';
import compress from 'vite-plugin-compression';

// https://astro.build/config
export default defineConfig({
  site: 'https://valstar.dev/',
  integrations: [mdx(), sitemap(), react()],
  markdown: {
    syntaxHighlight: 'prism',
  },
  vite: {
    plugins: [
      compress({ ext: '.br', algorithm: 'brotliCompress' }),
      compress({ ext: '.gz', algorithm: 'gzip' }),
    ],
    build: {
      // emptyOutDir: false, // Implement later as an option
    },
  },
});
