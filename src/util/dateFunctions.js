import dayjs from 'dayjs';

import utc from 'dayjs/plugin/utc';
import localizedFormat from 'dayjs/plugin/localizedFormat';
dayjs.extend(utc);
dayjs.extend(localizedFormat);

export const dateFormatBlog = (dt) => {
  return dayjs(dt).utc().format('ll');
};
