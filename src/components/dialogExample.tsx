import { useRef, useEffect, useState } from 'react';

function Dialog ({ children, open }) {
  const dialogRef = useRef(null);

  useEffect(() => {
    if (dialogRef.current) {
      if (open) {
        dialogRef.current.showModal();
      } else {
        dialogRef.current.close();
      }
    }
  }, [open]);

  return (
    <dialog ref={dialogRef}>
      {children}
    </dialog>
  );
}

export default function DialogWithButton () {
  const [open, setOpen] = useState(false);

  return (
    <>
      <button onClick={() => setOpen(true)}>Open Dialog</button>
      <Dialog open={open}>
        <h1>Dialog</h1>
        <p>This is a dialog</p>
        <button onClick={() => setOpen(false)}>Close Dialog</button>
      </Dialog>
    </>
  );
}
