import { useRef, useEffect, useState } from 'react';

function Dialog ({ children, open, onClose }) {
  const dialogRef = useRef(null);

  useEffect(() => {
    let justOpened = false;
    if (dialogRef.current) {
      if (open) {
        dialogRef.current.showModal();
        justOpened = true;
      } else {
        dialogRef.current.close();
      }
    }

    const onClick = (e: MouseEvent) => {
      // dont close on initial open/click
      if (!open || !dialogRef.current || justOpened) {
        justOpened = false;
        return;
      }

      // Bounding box as checking target wont work
      // if the dialog has padding (default browser behavior)
      var rect = dialogRef.current.getBoundingClientRect();
      var isInDialog =
        rect.top <= e.clientY &&
        e.clientY <= rect.top + rect.height &&
        rect.left <= e.clientX &&
        e.clientX <= rect.left + rect.width;

      if (!isInDialog) {
        onClose();
      }
    };

    if(open){
      document.addEventListener('click', onClick);
    }else{
      document.removeEventListener('click', onClick);
    }

    return () => {
      document.removeEventListener('click', onClick);
    };
  }, [open]);

  return (
    <dialog ref={dialogRef}>
      {children}
    </dialog>
  );
}

export default function DialogWithButton () {
  const [open, setOpen] = useState(false);

  return (
    <>
      <button onClick={() => setOpen(true)}>Open Dialog</button>
      <Dialog open={open} onClose={() => setOpen(false)}>
        <h1>Dialog</h1>
        <p>This is a dialog</p>
        <button onClick={() => setOpen(false)}>Close Dialog</button>
      </Dialog>
    </>
  );
}
