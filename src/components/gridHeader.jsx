import PropTypes from 'prop-types';

import gridavatar from '@mikevalstar/gridavatar';

const GridHeader = ({ className, seed, alt }) => {
  return <img className={className}
    alt={alt}
    download='hero-image.png'
    src={gridavatar(seed, { height: 140, width: 900, objSize: 13, type: 'hex', output: 'dataURL' })} />;
};

GridHeader.propTypes = {
  className: PropTypes.string,
  alt: PropTypes.string,
  seed: PropTypes.any.isRequired,
};

export default GridHeader;
