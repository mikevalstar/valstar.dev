import { useState } from 'react';
import PropTypes from 'prop-types';
import Styles from './markdownSimple.module.scss';

// Much copied from here: https://codepen.io/kvendrik/pen/bGKeEE

export const Preview = ({ value }) => {
  // Clean any html from teh user
  const cleanValue = value.replace(/<\/?[^>]+(>|$)/gi, '');

  // ul
  let final = cleanValue.replace(/^\s*\n\*/gm, '<ul>\n*');
  final = final.replace(/^(\*.+)\s*\n([^\*])/gm, '$1\n</ul>\n\n$2');
  final = final.replace(/^\*(.+)/gm, '<li>$1</li>');

  // ol
  final = final.replace(/^\s*\n\d\./gm, '<ol>\n1.');
  final = final.replace(/^(\d\..+)\s*\n([^\d\.])/gm, '$1\n</ol>\n\n$2');
  final = final.replace(/^\d\.(.+)/gm, '<li>$1</li>');

  // Headings
  final = final.replace(/[\#]{4}(.+)/g, '<h5>$1</h5>');
  final = final.replace(/[\#]{3}(.+)/g, '<h4>$1</h4>');
  final = final.replace(/[\#]{2}(.+)/g, '<h3>$1</h3>');
  final = final.replace(/[\#]{1}(.+)/g, '<h2>$1</h2>');

  // font styles
  final = final.replace(/[\*\_]{2}([^\*\_]+)[\*\_]{2}/g, '<b>$1</b>');
  final = final.replace(/[\*\_]{1}([^\*\_]+)[\*\_]{1}/g, '<i>$1</i>');
  final = final.replace(/[\~]{2}([^\~]+)[\~]{2}/g, '<del>$1</del>');

  // final pass for paragraphs
  final = final.replace(/^\s*(\n)?(.+)/gm, function (m) {
    return /\<(\/)?(h\d|ul|ol|li|blockquote|pre|img)/.test(m) ? m : '<p>' + m + '</p>';
  });

  return <div dangerouslySetInnerHTML={{ __html: final }}></div>;
};

Preview.propTypes = {
  value: PropTypes.string,
};

export const Editor = ({ keyUp, defaultValue }) => {
  return <textarea defaultValue={defaultValue} onKeyUp={e => { keyUp(e.target.value); }} />;
};

Editor.propTypes = {
  defaultValue: PropTypes.string,
  keyUp: PropTypes.func,
};

const defaultVal = `
# Title
## Subtitle

* list
* list 2

and some more _text_ that is **styled** a ~~little bit~~ yeah
`;

export const Combo = () => {
  const [md, setMD] = useState(defaultVal);

  return <div className={Styles.mds}>
    <div>
      <h3>Editor</h3>
      <Editor defaultValue={md} keyUp={(v) => { setMD(v); }} />
    </div>
    <div>
      <h3>Preview</h3>
      <Preview value={md} />
    </div>
  </div>;
};

export default Combo;
