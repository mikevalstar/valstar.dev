import rss from '@astrojs/rss';
import { SITE_TITLE, SITE_DESCRIPTION } from '../config';

const itemsGlob = await import.meta.glob('./blog/**/*.{md,mdx}', { eager: true });
const itemValues = Object.values(itemsGlob);
const items = itemValues.map(m => ({
  link: m.url,
  title: m.frontmatter.title,
  pubDate: m.frontmatter.pubDate,
  description: m.frontmatter.description,
}));

export const get = () =>
  rss({
    title: SITE_TITLE,
    description: SITE_DESCRIPTION,
    site: import.meta.env.SITE,
    stylesheet: '/pretty-feed-vs.xsl',
    items,
  });
