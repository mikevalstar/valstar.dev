---
layout: "../../layouts/BlogBasic.astro"
title: "You don't need a CMS"
pubDate: "2023-07-16"
modDate: "2023-07-16"
heroImage: "/img/working_plans.png"
heroImageAlt: "A man working on plans - by Midjourney"
description: |
    When you are going to build a website, you're going need a way to manage content, so you need a CMS right?
---

If you have access to a web developer, weather that means you work at a company with developers, or you work on a development team yourself. You almost certainly don't need a CMS. I would go so far as saying having a CMS makes every aspect of your website worse; Time to market, time to add a new component or page, number of bugs, your user's experience and even modifying an existing page or component.

## When you do need a CMS

Everything has an exception, and of course there are good reasons to have a CMS. However that list is very short: 

- You don't have access to a developer (and aren't one yourself)
- You have full time editor(s)

And that's it. 

If when your project is "ready" you don't have access to a developer anymore then a CMS allows for a non-technical person to continue to edit the site, which you of course need. This usually applies to small businesses or personal websites, and in those cases you can either get a cheap template for something like Wordpress or use a service like Squarespace, this is probably all you need.

Additionally if you have 1 or more full time editors writing then a CMS is of course going to be required; however in this case I would recommend a headless CMS and we will get into that later.

## What do You Build Then?

You still need to code a website, and you can use modern or even not-so-modern techniques to be able to build reusable components in an easy and simple manner.

I would also argue your development team can already do this, and in fact would prefer it to your current plan to build a CMS based website.

Before moving on to why all the arguments I've ever heard for why you need a CMS, let me tell you about a website I built for a client:

A few years ago me and my team built a marketing website for a client, it was approximately 50 pages, with about 15 unique layouts and have probably 100 reusable components.

We have dozens of meetings with the client, we make, remake, and tweak pages, components, and the editor to make sure every component can work in any layout, that ultimately everything works everywhere.

5 months later, website launches, client is happy, we're happy, job done! Then about a week later, the client emails us: "hey we want to change the title of the about us page to something else". 
Cool, we email back: "That's super easy, just login to the CMS, go to the page you want and you can edit the title and hit save!". 
Their response? "We don't want to make the change, thats what we hired you for, and CMS should make that change easier for you!".

This is not the first time I've had this conversation with a client, or a similar conversation. 
We built the CMS for you!, or we built the CMS so that it's cheaper for us to make content changes in the future.
Well let me let you in on an industry secret: for a developer, especially with a modern development toolkit, editing the code will be faster to make the change in the CMS 9 times out of 10.

## Time to Market
Let me start with the biggest reason for not using a CMS; frankly you will get your website to market quicker. Phenomenally quicker!

If you are building a website there are a number of tools that will allow you to either generate a semi-dynamic website like next.js, rocket, django and many others or you can generate a fully static website with something like Astro while still being able to use modern templating tools such as React, Solid, Svelte, Lit, Vue, and probably any other technology that suits your team. 

Using standard development tools like these is faster then working with a CMS for several reasons; 
most notably when designing your page components they only need to work with the content that is actually going to be there. 
When building for a CMS you generally want to design your components to be both setup to work with the CMS framework, but also that they work generically. This will mean when a layout does change you will have additional work, but only a fraction of your components will need to be updated compared to all of them.

You also generally need to test your components in the CMS, which is a slow process, as you need to make sure that your components work in every layout and based on content that may be very different from what are in your designs.

This may sound less desirable, but when you have a specific design, and you know exactly what content is going to be on the page, you can make things simpler and faster. And as I will cover below you can modify these components fast and easily.

Additionally when writing custom code you will be less worried about making 2 or 3 variations of a component, where as in a CMS you generally end up building your components like a bit of a swiss army knife instead of having variants of the same/similar components, ultimately making them far more complicated.

When all is said and done, developing a website using custom code will be faster as you don't need to support a CMS and I would hazard a guess that you will be able to get to market 3-4 times faster. And if you don't believe me, talk to one of your developers and ask them how long it would take to build a website using a CMS vs using a modern development toolkit, I suspect they will tell you the same thing.

## Adding New Content

Adding new content is very similar to time to market, but I think it's worth calling out separately. 

When you are adding new content to a website, you are often reusing components and layouts that already exist. This is where a CMS will really shine, right? they do until you want to add a new component, or you use a component in an unexpected way, then you are back to needing a developer to assist in that pages creation. You are now back to all of the same problems you get with time to market. 

When the new content you want to add can be built using existing components, a CMS would in many cases be somewhat faster to add the content. However I think many people overestimate how long it takes to add content manually to a website. Assuming all of the components already exist I cannot think of a page I have ever built that would take more then a couple hours to add the content to, and it would have taken equally long to add to the CMS due to all the random components that you need to choose and click all the options for in the various components as well as the usually terrible WYSIWYG editor that you need to use to add content. 

In this type of case I can see a CMS being faster, but I would argue that it's not worth the cost of the CMS once you factor in all the time spent in the initial development of the website.

## Modifying Content

For modifying content A CMS is going to take basically the same amount of time as modifying the code directly. File search is a thing, and it's fast, like super fast. Often one of the common changes I've been asked to do for a website is to change a product name, or some specific wording around a product. Finding all of the instances of that word or phrase in the code is trivial, while doing so in a CMS, depending on the CMS can be a bit of a pain.

## Maintenance

Maintenance is a big one, and I think it's one of the biggest reasons to not use a CMS. Often times when maintaining a website you are going to either want to make changes to components and how they work or or you are doing a revamp of your website. 

For many websites a redesign happens once every 3 years or so. https://www.walkersands.com/why-companies-redesign-websites-every-three-years-on-average-or-do-they/ 

When you are doing a redesign you are going to want to change a lot of things, and you are going to want to change them fast. Using custom code can often allow you to make these changes in parallel with your existing design or at the very least get you a faster to market complete rewrite.

## Bugs

Simply put the more code involved with any project the higher the probability of bugs. This is true for both custom code and CMS code, however when you are using a CMS you creating far more complex components then you would if you were using custom code where they would match the design exactly and not be required to work in interesting situations.

## User Experience

A big one for me is the user experience for the end users of your website. I have yet to see a CMS that doesn't add a significant amount of bloat to the website. This bloat can be in the form of extra javascript, extra css, or even just extra html. And this bloat simply slows down your website.

Now this blog is not exactly a great example of a website you might build, it's a simple blog. But the download size (not including images) for this page is somewhere under 100kb of html, css and javascript (gzipped) plus about 100kb of external source. In contrast any given article on medium is going to be well over 1mb of html, css and javascript (gzipped) plus other items it loads up later; and medium is a very streamlined website for what it is, but it is built similarly to a CMS.

Much of the added bloat will slow down the perceived speed of your website to the user. It may still be fast, but not as blazingly fast as it could be.

## Overall Costs

So if we take into account all of the above, what does it cost to build with a CMS vs without a CMS. Well I'm going to make some assumptions here, I think they are reasonable, and because I dont want to be tied to dollars as those will differ over time and based on your location. Lets work with hours instead.

So lets say you a have a reasonably complicated website, with about 200 pages, built from 100 different components and about 20 layouts; or a small-medium sized website. If I were to estimate this (developer hours only) for a CMS (lets go with Wordpress) I would estimate about 3 days per component, 5 day per layout and 1 day per page along with a couple of days to get your basic website setup:

* 5 days for basic setup
* 100 components * 3 days = 300 days
* 20 layouts * 5 days = 100 days
* 200 pages * 1 day = 200 days

For a total of 605 days of development time. 

For a custom version of this website I would estimate about 1 day per component, 1 day per layout and a conservative 1 day per page and lets allow for a much more complicated setup process:

* 50 days for basic setup
* 100 components * 1 days = 100 days
* 20 layouts * 1 days = 20 days
* 200 pages * 0.5 days = 100 days

This works out to 270 days of development time; nearly half the time of the CMS version.

Now lets say for the custom version you will need 1 person full time for the duration of 3 years to maintain the website, and for the CMS version you will need 1 non-technical person part time for the duration of 3 years to maintain the website. This adds 750 days to the custom version and lets say 300 days days to the CMS version.

This ends up giving us a total of 1020 days for the custom version and 905 days for the CMS version.

This looks like the CMS version is cheaper, but we have not accounted for any bug fixes, new components or layouts. If you just start adding or modifying a few components a year you can easily add more then the 100 days of development time to the CMS version, and if you are like most of my clients over the years this means waiting on changes and updates until you have enough to make it worth it to have a developer re-onboarded to the project to make changes.

I will also add that a full time developer on the website is most likely not necessary and you can get away with a part time developer if you have other work for them, but if you have a full time developer you can experiment more and try new things, or just add more content with your "free" time.

## A Middle Ground

A middle ground to this is to use a headless CMS to manage your content. With services like contentful, or strapi or one of the others you can manage your content in a CMS and then use a custom website to display that content. This allows you to get the best of both worlds, you can have a custom website that is fast and easy to maintain, but you can also have a CMS that allows you to manage your content without having to go through a developer.

There will be some tradeoffs in this case, as you will limited in you layouts, and you will need some development time to make these kinds of changes, but you will still be able to make them faster then you would with a standard CMS.

My personal opinion is that you don't even need a headless CMS; but this can make for a google middle ground and is usually what I suggest to clients these days.

## Why I'm Wrong
After all I've said, there will be those of you that disagree with me, and there are some good reasons why you are right and I am wrong:

Your company cares more about operational costs then capital costs. I've seen so many companies that can rationalize a 1 time cost of $1,000,000 but cannot rationalize a yearly cost of $100,000 for a 4 years. If this is your company, then a CMS is probably the right choice for you.

You, or the people at your company want to be able to make a change to the website and see it "instantly" on the website. A CMS allows this, but a good CI/CD setup for a custom site can take upwards of 5 minutes to deploy a change to the website. If you need to see changes instantly, then a CMS is probably the right choice for you.

You believe you will not need to make any changes to the website beyond a couple word changes and you have no way of hiring someone for the 8-24 hours a month required to make those changes. If this is the case then a CMS is probably the right choice for you.

I'm sure your own company has it's own reasons for using a CMS, and I'm sure they are valid reasons. But I hope that you will at least consider the cost of that CMS vs the infinite customizabilty of a custom website when you're next redesigning your website.
