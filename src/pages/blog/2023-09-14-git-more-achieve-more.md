---
layout: "../../layouts/BlogBasic.astro"
title: "Git More, Achieve More"
pubDate: "2023-09-14"
modDate: "2023-09-14"
heroImage: "/img/nvim-gitlog.png"
heroImageAlt: "git log of neovim"
description: |
  Committing more often can speed up your development and improve your workflow, build your applications incrementally and keep track of your progress.
---
## You Aren't Committing Enough

In my day job I work with a variety of programmers with a variety of skill levels. One thing I've noticed is that most programmers seem to commit to their git repo very infrequently and the less experienced programmers even more so. I've seen some programmers go days or even weeks without committing anything because they are still working on that feature or bug.

You should be committing more often, several times per hour if the feature is easy, and a few times per day if what you are working in is more complex and in the case of complex problems you should probably have a few failed branches before you get it right.

## Why You Should Commit More Often

But what is the point of committing more often if you're just going to have to squash them all together later? wont a messy commit history make you look bad? Aren't you wasting time?

Well lets go over some of the benefits first:

### Easier to Roll Back

Of course you can always `ctrl+z` a few hundred times to get back to where you were, having specific checkpoints in your code that you can roll back to is much easier and allows you to be confident you have reversed the correct changes and you have no dangling changes that you forgot about.

Git helps you keep track of these checkpoints and even allows for branching while you're trying to find the correct solution to your issue. 

_for the vim/neovim users out there you can of course get a similar effect with the `undotree` [plugin](https://github.com/mbbill/undotree) but this is not as robust as git and you can't share it with your team_

### Small Accomplishments

Committing often can also help you generate measurable progress towards your goal, especially on a larger task. I personally find it can help you from becoming overwhelmed or discouraged when I can see that I'm making progress towards your goal.

### Asking for Assistance or Feedback

Should you need to ask for help either through shoulder surfing or a remote code review, having a commit history that shows your task in discreet steps can help your fellow developer understand what you are trying to accomplish would having to understand the entire changeset at once.

### Show Your Thought Process

When you are finishing your task and you are ready to merge your changes into the main branch, having a commit history that shows your thought process can help your fellow developers understand why you made the changes you did and how the changes relate to each other.

## The Perils of Not Committing Often Enough

The downsides to committing infrequently can easily be summed up with lost work, or at least lost time trying to recover lost work. How often have you run a `git reset` because you've broken something and cant figure out what you broke, or lost an hour trying to undo a change while keeping that other change you do want to keep?

## How to Commit More Often

So hopefully I've convinced you to to hit run that `git commit` command a little more often, but when should you be committing?

### Progressive Improvements

Start by focusing on progressive improvements towards your goal; whenever you've completed a small step that improves what you are working on commit that and be sure to write a good but short commit message so you can easily understand what you were doing and reference it later. 

### Atomic Features
Try to break up your large feature into smaller atomic features that can be completed in a few hours or less. This can help break up your task and may even help you plan out the larger feature with less effort.

## Cleaning Up Your History

Okay so now you have a commit history that looks like someone threw a bunch of spaghetti at the wall, how do you clean it up?

You can use `git rebase` to squash your commits together into a single commit, or even reword your commit messages to make them more clear. If you're using a GUI for git it can help with this, or you can check check out [this great article on rebasing](https://medium.com/@slamflipstrom/a-beginners-guide-to-squashing-commits-with-git-rebase-8185cf6e62ec)

You can also leave this squashing to be done within your pull request (it's just a checkbox on gitlab), which is what I usually do.

## Celebrate Your Accomplishments

Although this may not hold true for everyone I personally find that by saving and committing more often I can feel more accomplished in my day, or my time between meetings. I treat git like an incremental game or a progressive enhancement game like Factorio, always making improvements so that the factory and my application can grow!
