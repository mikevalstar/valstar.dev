---
layout: "../../layouts/BlogBasic.astro"
title: "Updating a Gorgon"
pubDate: "2023-09-06"
modDate: "2023-09-06"
heroImage: "/img/gorgon.png"
heroImageAlt: "Gorgon logo"
description: |
  I made some updates to Gorgon.js, allowing you to add hooks into the caching system allowing for a larger plugin space.
---
## Gorgon.js

I have added a few new features to Gorgon.js since I last talked about it last [November](/blog/2022-11-13-grogonjs).

### Hooks

Gorgon now has a hook system that allows you to add in your own custom hooks to the caching system. This allows you to do things like add in a custom logger, or in my case allows me to write a tool to sync caches between multiple servers.

Check out https://gorgonjs.dev/docs/hooks/ for more information.

### The Future

I'm hoping to write this new syncing example in the next month or so, but the hook system is ready to go for anyone else to write their own plugins.