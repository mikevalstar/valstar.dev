---
layout: "../../layouts/BlogBasic.astro"
title: "A Fresh Start"
pubDate: "2022-09-03"
modDate: "2022-09-03"
heroImage: "/img/hexagons-first-post.png"
heroImageAlt: "Generated with Midjourney: Dark hexagons"
description: "It's been a few years, and my older blog has been neglected and the contents old and crusty. Lets get a fresh start going again!"
---

It's been a few years since I last posted to my blog, this has mainly been due to my priorities being shifted; 
I got married, I helped a few friends with their website, and I devoted much of my time to an internal company project that I was very interested in that is now finished.

I've wiped the old blog data as the technologies are no longer in common use and are not super relevant to what I want to write about now. I have some ideas for new posts and some plans for some personal projects to populate this and to help scratch that programming itch. 

The new website is all setup now, I have all the basics going... however none of my usual easter eggs or fun little things are in place. So I'll be adding those over the next few months as well. (I also need to clean up [the code](https://gitlab.com/mikevalstar/valstar.dev) a little bit)