---
layout: "../../layouts/BlogBasic.astro"
title: "Federated GraphQL - Part 1 - Enterprise GraphQL"
draft: true
pubDate: "2022-10-15"
modDate: "2022-10-15"
heroImageAlt: "Federated GraphQL part1"
description: |
  Building a Federated GraphQL service at large companies can be a disaster if you don't set it up properly. 
  How do you ensure you are building a scalable and maintainable GraphQL backend?
---

## Introduction

GraphQL has gained a ton of traction and usage for public client facing applications, and this I would say is largely because it allows for multiple clients (most specifically mobile) to each define their own queries and data returned to meet their particular needs. 

In the last few years GraphQL has stabilized and is starting to be looked at in enterprise environments. But enterprise environments have some different problems they need to solve and a Federated Graph can solve these problems as well.

## Federated GraphQL

### The Problems

### Solving Enterprise Problems


