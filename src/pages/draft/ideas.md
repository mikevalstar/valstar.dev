# Idea List

- Write it twice - talk about how you should write your code twice, once for your understanding and once for the computer's understanding
- Fast is better then correct - talk about how people only care that something is delivered fast, not that it's correct or runs fast
- 